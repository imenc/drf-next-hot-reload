# docman-app
[![forthebadge made-with-python](http://ForTheBadge.com/images/badges/made-with-python.svg)](https://www.python.org/)

[![Visual Studio Code](https://img.shields.io/badge/--007ACC?logo=visual%20studio%20code&logoColor=ffffff)](https://code.visualstudio.com/)  &
[![official JetBrains project](http://jb.gg/badges/official.svg)](https://www.jetbrains.com/pycharm/)



## Features
- Docman App is kind of app which its purpose is to record document versioning

## Techstack
Tech stack using in **docman-app** :
- [Python](https://www.python.org) - Back end main programming language
- [Django](https://www.djangoproject.com) - Python framework
- [DRF](https://www.django-rest-framework.org) - Rest API framework
- [ReactJS](https://reactjs.org) - Front End UI library
- [NextJS](https://nextjs.org) - React framework for production
- [MySQL](https://www.mysql.com) - RDBMS
- [Docker](https://www.docker.com) - Containerization Platform

## Developing docman-app on your windows/mac OS
> Note: Firstly install & run docker desktop on host. Git enabled on terminal is a must

```sh
git https://gitlab.com/imenc/docman-app.git
cd docman-app
docker-compose build
docker-compose up
```
> To shutdown the container, after pressing ctrl + C do the following

```shell
docker-compose down
```

## To do list
- [x] Create docker compose for App (BackEnd)
- [x] Create docker compose for Fe (FrontEnd)
- [x] Create docker compose for Db (MySql)
- [x] Create hashed id base models
- [x] Change default user name login to email login
- [x] Add /api/user/login end point

## Deployment Environment
- [http://localhost:8000/admin/](http://localhost:8000/admin/) - Development Server

## Python console command for creating fixtures
```python
./manage.py dumpdata auth.Group --indent 4 > fixtures/auth_group.json
./manage.py dumpdata core.User --indent 4 > fix
./manage.py dumpdata core.Provinces --indent 4 > fixtures/core_provinces.json
./manage.py dumpdata core.Cities --indent 4 > fixtures/core_cities.json  
./manage.py dumpdata core.Districts --indent 4 > fixtures/core_districts.json
./manage.py dumpdata core.Subdistricts --indent 4 > fixtures/core_sub_districts.json
```

## Docker compose command for app migration related
```shell
docker-compose run --rm app sh -c "python manage.py makemigrations"
docker-compose run --rm app sh -c "python manage.py migrate"
```

## Docker compose command for load data fixtures
```shell
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/auth_group.json --app auth.Group"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/core_user.json --app core.User"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/core_provinces.json --app core.Provinces"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/core_cities.json --app core.Cities"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/core_districts.json --app core.Districts"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/core_sub_districts.json --app core.Subdistricts"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/blog_article_cat.json --app blog.ArticleCat"
docker-compose run --rm app sh -c "python manage.py loaddata fixtures/blog_article.json --app blog.Article"
```

## Django-admin command for inspectdb
```shell
docker-compose run --rm app sh -c "python manage.py inspectdb > sql/models.py"
```

## Django-admin command for create project
```shell
django-admin startproject backend
```

## Django-admin command for create apps
```shell
python manage.py startapp polls
```

## Check Django version
```shell
workon proshop
python -m django --version
```

## Docker compose command for linting
```shell
docker-compose run --rm app sh -c "flake8"
```


# Docker Compose Basic Command
```shell
// create and start containers
docker-compose up  
// start services with detached mode
docker-compose -d up  
// start specific service
docker-compose up <service-name>  
// list images
docker-compose images  
// list containers
docker-compose ps  
// start service
docker-compose start  
// stop services
docker-compose stop  
// display running containers
docker-compose top  
// kill services
docker-compose kill  
// remove stopped containers
docker-compose rm  
// stop all contaners and remove images, volumes
docker-compose down  
```
