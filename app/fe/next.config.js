/** @type {import('next').NextConfig} */
module.exports = {
    env: {
        API_URL: 'http://localhost:8000',
    },
    images: {
        domains: ['localhost'],
    }    
}
